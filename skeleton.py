#>> these are my comments and not part of the skeleton
#!/usr/bin/env python
#>> above line is optional, used by the shell not python
#>> for running scripts on a POSIX shell (*nix OS, WSL on Windows, bash/fish/zsh)
#>> you will often see an encoding line right but also optional
#>>
#>> The Gory Details:
#>> PEP 8 Style Guide for Python Code
#>>  https://www.python.org/dev/peps/pep-0008/
#>> PEP 20 -- The Zen of Python
#>>  https://www.python.org/dev/peps/pep-0020/

"""
Long comments are triple qouted and ignored. Allowing you to
really take some space to get into detail on the fucntions and
purpose of your code. Or, finally write out those childhood issues
you've been denying for 20 years. Okay... easy, You could also
plan a vacation.

Author: Bob Skeleton
Other stuff..

"""
#>> leave a space and perform any imports

import os
import argeparse


#>> two spaces, functions next
def get_data(file):
    pass

def process_data(data):
    pass

def put_data(data, file):
    pass

#>> main() first function ran (entry point)
#>> Can be any name you want, common: run, app, cli, (at least in tutorials)
def main():
    #>> interactions with users
    #>> processing command line options
    print('Running')

    in_data_file = 'fake_file_name.in'
    out_data_file = 'other_fake_file_name.out'

    print('getting data')
    data = get_data(file=in_data_file)

    print('processing data')
    process_data(data)

    print('putting data')
    put_data(data=data, file=out_data_file)

    print('main end')

#>> WTF? - This if enables the code to be run interactively or
#>> imported by other python scripts and modules for reuse. 
if '__name__' == '__main__':
    main()
