#!/usr/bin/env python3

import string
import random
import argparse

def cli_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('--cnt', type=int)

    return parser.parse_args()


def pw_gen(cnt):
    chars = string.ascii_letters + string.digits + string.punctuation
    str_password = str()

    for i in range(cnt):
        str_password = '{}{}'.format(str_password, random.choice(chars))


def main():

    opts = cli_args()

    pw_gen(opts.cnt)

    print(str_password)

if __name__ == "__main__":
    main()
